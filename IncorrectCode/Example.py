import rollbar
rollbar.init('', 'production')  # access_token, environment

try:
    main_app_loop()
except IOError:
    rollbar.report_message('Got an IOError in the main loop', 'warning')
except:
    # catch-all
    rollbar.report_exc_info()
    # equivalent to rollbar.report_exc_info(sys.exc_info())

